# Crosshair loader

When designing and developing to the grid, it can be very useful to ensure alignment between various layout components on the page. <br>
One easy way to do this is to use a crosshair cursor. <br>
"Crosshair loader" is a JavaScript bookmarklet which provides exactly this on any web-page you wish.

## Usage
**Using Crosshair is extremely simple:** <br>
Drag the Crosshair link on this page to your bookmark bar (or add it to your bookmarks by right clicking the link and selected 'Add to bookmarks/favourites'): https://natizyskunk.gitlab.io/crosshair-loader


## Crosshair
Load any web-page you want <br>
Click Crosshair in your bookmark bar <br>
Move the mouse around the web-page and the crosshair will follow. <br>
The usage of Crosshair is as simple as loading it in a web-page, as described above. The crosshair, including a small information box about the x and y co-ordinates of the mouse on the page. <br>

## Supported browsers
**The following browsers are supported in Rule:** <br>
Internet Explorer 7 <br>
Firefox 2+ <br>
Safari 2.0.4+ <br>
Opera 9.2 <br>
There is no IE6 support.
